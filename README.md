# Chat SD



> Desenvolvido por: 

|           Nome           |   R.A.   |
|:------------------------:|:--------:|
| Anna Paula Braz          | 16211490 |
| Cainã Colonezi Rodrigues | 15113510 |
| Diego Oliveira Santos    | 17100262 |
| Hassan Jacoboski         | 16114165 |
| Isabel Noguti            | 16211507 |
| Rodrigo Baptista Correa  | 17100422 |

---------

Requisitos:

Python 2.7;

Instale o [Virtual Box Oracle](https://www.virtualbox.org/wiki/Downloads);

Instale o [Vagrant](https://www.vagrantup.com/downloads.html);

Efetue o clone deste projeto em uma pasta;

Entre na pasta e pelo CMD execute `vagrant up`;

Após efetuar o aprovisionamento, execute `vagrant ssh`;

Ele irá entrar na máquina virtual criada.

Execute `cd ../../vagrant` e depois `python chat_server.py 0.0.0.0 8002`;

Em outro terminal execute novamente `vagrant ssh`, `cd ../../vagrant` e agora execute `python chat_client.py 0.0.0.0 8002 <Seu Nome>`


### Funcionamento do sistema


Este leve sistema de chat desenvolvido em python (no caso, apenas uma sala com bradcasting global da mensagem) baseia-se em 2 conceitos chaves para um app de comunicação em um sistema distribuído:

- [***Sockets***][3],
- [***Multi-threading***][2].

Vale lembrar que esses scripts são feitos para funcionamento em sistemas UNIX, dado a possibilidade de efetuar pipe do console. No windows é necessário efetuar mais mudanças para isso, pois _não é feito o redirecionamento de input_.

#### Detalhando o funcionamento:

- ***Socket***
> Pense num socket com um ponto de comunicação bi-direcional estabelecido entre um servidor e vários clientes.
O socket, no lado do servidor, associa-se a uma porta de rede do hardware.
Todo cliente que tiver um socket associado pode se conectar com o servidor naquele socket e porta.

- ***Multi-threading***
> Um thread é um subprocesso (no caso do exemplo usado neste programa, a documentação do python o chama de LWP, [_light weight process ou task_][1]) que executa uma série de comandos isoladamente de outros threads.
Dessa forma, toda vez que um cliente conecta-se ao servidor, um thread é criado para aquele cliente, de certa forma isolando e identificando aquele cliente. Para atingir isso, será necessário o uso de 2 scripts
(_server_ e _client_). O segundo deverá ser executado em cada cliente.

- **Exceução dos scripts**
      
- Server side script:  
O servidor tentará estabelecer um socket e irá vinculá-lo a um endereço IP e uma porta especificada pelo usuário.
```python
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
if len(sys.argv) != 3:
    print "Uso correto: script, endereco IP, porta"
    exit()
IP_address = str(sys.argv[1])
Port = int(sys.argv[2])
# Atribui o IP e porta ao socket.
server.bind((IP_address, Port)) 
# Resolvemos ajustar para escutar apenas 100 clientes. Pode-se mudar para mais conexões.
server.listen(100)
list_of_clients=[]
```  
Com o script aberto e executando, ele estará esperando requisições de conexão e irá atribuir um thread para cada usuário que se conecte. A mensagem sendo recebida, será reenviada para todos os clientes conectados.
Caso ocorra alguma excessão, aquele thread será interrompido e desconectado. 

---------
    
- Client side script:

O script para cliente tentará acessar o socket criado no servido no IP e porta especificadas. Quando conectar, ele ficará continuamente checando se há alguma entrada do servidor ou mesmo do cliente, conforme redireciona as saídas.

Dessa forma, podemos ilustrar como um thread atua assim:

![UNIX Threads](./images/lwp.jpeg)

[1]: https://docs.python.org/2/library/thread.html
[2]: https://en.wikipedia.org/wiki/Light-weight_process
[3]: https://pt.wikipedia.org/wiki/Soquete_de_rede
Imagem: http://www.cs.miami.edu/home/visser/Courses/CSC322-09S/Content/UNIXProgramming/UNIXThreads.shtml


----------  
### Referência: 

https://www.geeksforgeeks.org/simple-chat-room-using-python/